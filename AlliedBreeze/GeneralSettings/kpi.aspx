﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="kpi.aspx.cs" Inherits="AlliedBreeze.kpi" %>


<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>AlliedBreeze</title>
    <meta name="viewport" content="">
    <meta name="description" content="">
    <meta property="og:url" content="">
    <meta property="og:type" content="">
    <meta property="og:title" content="">
    <meta property="og:description" content="">
    <meta property="og:image" content="">
    <meta name="twitter:card" content="">
    <meta name="twitter:site" content="">
    <meta name="twitter:creator" content="">
    <meta name="twitter:title" content="">
    <meta name="twitter:description" content="">
    <meta name="twitter:image" content="">
    <link rel="apple-touch-icon" sizes="180x180" href="">
    <link rel="icon" type="image/png" href="" sizes="32x32">
    <link rel="icon" type="image/png" href="" sizes="16x16">
    <link rel="manifest" href="manifest.json">
    <link rel="mask-icon" href="safari-pinned-tab.svg" color="#001C98">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,400italic,500,700">
    <link rel="stylesheet" href="contents/css/vendor.min.css">
    <link rel="stylesheet" href="contents/css/elephant.min.css">
    <link rel="stylesheet" href="contents/css/application.min.css">
      <link rel="stylesheet" href="contents/css/demo.min.css">
  </head>
  <body class="layout layout-header-fixed">
    <div class="layout-header">
      <div class="navbar navbar-default" style = "background-color: #001C98; height:75px">
        <div class="navbar-header" style = "background-color: #001C98">
          <a class="navbar-brand navbar-brand-center" href="audience.html">
            <img class="navbar-brand-logo" src="contents/img/loo.png" alt="" style = "margin-top: -15px; height:72px; width:76px">
          </a>
          <button class="navbar-toggler visible-xs-block collapsed" type="button" data-toggle="collapse" data-target="#sidenav">
            <span class="sr-only">Toggle navigation</span>
            <span class="bars">
              <span class="bar-line bar-line-1 out"></span>
              <span class="bar-line bar-line-2 out"></span>
              <span class="bar-line bar-line-3 out"></span>
            </span>
            <span class="bars bars-x">
              <span class="bar-line bar-line-4"></span>
              <span class="bar-line bar-line-5"></span>
            </span>
          </button>
          <button class="navbar-toggler visible-xs-block collapsed" type="button" data-toggle="collapse" data-target="#navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="arrow-up"></span>
            <span class="ellipsis ellipsis-vertical">
              <img class="ellipsis-object" width="32" height="32" src="contents/img/user.png" alt="Teddy Wilson">
            </span>
          </button>
        </div>
        <div class="navbar-toggleable">
          <nav id="navbar" class="navbar-collapse collapse">
            <button class="sidenav-toggler hidden-xs" title="Collapse sidenav ( [ )" aria-expanded="true" type="button">
              <span class="sr-only">Toggle navigation</span>
              <span class="bars">
                <span class="bar-line bar-line-1 out"></span>
                <span class="bar-line bar-line-2 out"></span>
                <span class="bar-line bar-line-3 out"></span>
                <span class="bar-line bar-line-4 in"></span>
                <span class="bar-line bar-line-5 in"></span>
                <span class="bar-line bar-line-6 in"></span>
              </span>
            </button>
            <ul class="nav navbar-nav navbar-right">
              <li class="visible-xs-block">
                <h4 class="navbar-text text-center">Hi, Ama Mantebea</h4>
              </li>


              <li class="dropdown hidden-xs">
                <button class="navbar-account-btn" data-toggle="dropdown" aria-haspopup="true" style="margin-top:15px">
                  <img class="rounded" width="36" height="36" src="contents/img/user.png" alt=""> &nbsp &nbsp Ama Mantebea | <a href="login-1.html"><span style = "color: #fff"><i class = "icon icon-lock"> </i> Sign out</span></a>

                </button>


              </li>


              <li class="visible-xs-block">
                <a href="login-1.html">
                  <span class="icon icon-power-off icon-lg icon-fw"></span>
                  Sign out
                </a>
              </li>
            </ul>
          </nav>
        </div>
      </div>
    </div>

    <div class="layout-main" >
      <div class="layout-sidebar" >
        <div class="layout-sidebar-backdrop" style="background-color:white"></div>
        <div class="layout-sidebar-body" style="background-color:white">
          <div class="custom-scrollbar">
            <nav id="sidenav" class="sidenav-collapse collapse" style="background-color:white">
              <ul class="sidenav">
                  <br />
                <li class="sidenav-heading">General Settings</li>
                <li class="sidenav-item  ">
                  <a href="dashboard-1.html" aria-haspopup="true">
                    <span class="sidenav-icon icon icon-users"></span>
                    <span class="sidenav-label" style="font-size: 11px">Staff Management</span>
                  </a>
                                 </li>
                  <li class="sidenav-item  ">
                      <a href="dashboard-1.html" aria-haspopup="true">
                          <span class="sidenav-icon icon icon-briefcase"></span>
                          <span class="sidenav-label" style="font-size: 11px">Job Roles</span>
                      </a>
                  </li>
                   <li class="sidenav-item ">
                      <a href="dashboard-1.html" aria-haspopup="true">
                          <span class="sidenav-icon icon icon-circle"></span>
                          <span class="sidenav-label" style="font-size: 11px"> Indicators</span>
                      </a>
                  </li>
                   <li class="sidenav-item  active">
                      <a href="dashboard-1.html" aria-haspopup="true">
                          <span class="sidenav-icon icon icon-chevron-down"></span>
                          <span class="sidenav-label" style="font-size: 11px">Key Performance Indicators</span>
                      </a>
                  </li>
                    <li class="sidenav-item  ">
                      <a href="dashboard-1.html" aria-haspopup="true">
                          <span class="sidenav-icon icon  icon-map"></span>
                          <span class="sidenav-label" style="font-size: 11px">Sectorial &amp Branch Management</span>
                      </a>
                  </li>

                <li class="sidenav-heading">Data Input Management</li>
                <li class="sidenav-item ">
                  <a href="#" aria-haspopup="true">
                    <span class="sidenav-icon icon icon-contao"></span>
                    <span class="sidenav-label" style="font-size: 11px">Risk & Controls</span>
                  </a>

                </li>
                <li class="sidenav-item ">
                  <a href="#" aria-haspopup="true">
                    <span class="sidenav-icon icon icon-users"></span>
                    <span class="sidenav-label" style="font-size: 11px">Customer Service</span>
                  </a>

                </li>

                   <li class="sidenav-item ">
                  <a href="#" aria-haspopup="true">
                    <span class="sidenav-icon icon icon-thumbs-up"></span>
                    <span class="sidenav-label" style="font-size: 11px">Qualitative Objectives</span>
                  </a>

                </li>

                  <li class="sidenav-heading">Reporting Portals</li>
                  <li class="sidenav-item ">
                      <a href="#" aria-haspopup="true">
                          <span class="sidenav-icon icon icon-bell-o"></span>
                          <span class="sidenav-label" style="font-size: 11px">Individual</span>
                      </a>

                  </li>
                  <li class="sidenav-item ">
                      <a href="#" aria-haspopup="true">
                          <span class="sidenav-icon icon icon-list"></span>
                          <span class="sidenav-label" style="font-size: 11px">Branchwise</span>
                      </a>

                  </li>

                    <li class="sidenav-item ">
                      <a href="#" aria-haspopup="true">
                          <span class="sidenav-icon icon icon-list"></span>
                          <span class="sidenav-label" style="font-size: 11px">Departmental</span>
                      </a>

                  </li>

                    <li class="sidenav-item ">
                      <a href="#" aria-haspopup="true">
                          <span class="sidenav-icon icon icon-list"></span>
                          <span class="sidenav-label" style="font-size: 11px">Sectorial</span>
                      </a>

                  </li>

                    <li class="sidenav-item ">
                      <a href="#" aria-haspopup="true">
                          <span class="sidenav-icon icon icon-list"></span>
                          <span class="sidenav-label" style="font-size: 11px">Executive</span>
                      </a>

                  </li>



              </ul>
            </nav>
          </div>
        </div>
      </div>
      <div class="layout-content">
      <div class="layout-content-body">
          <br />
          <div class="title-bar">

              <h1 class="title-bar-title">
                  <span class="d-ib pull-left">Key Performance Indicators &nbsp &nbsp </span>
                   <button class="pull-left btn btn-pill btn-success" style = "text-transform: capitalize; background-color: #001C98; border-color: #001C98 " data-toggle="modal" data-target="#createSector" type="button"> <i class = "icon icon-plus"></i> &nbsp New KPI</button>

              </h1>
              <p class="title-bar-description">
                  <small  style ="color:#14692E">Management</small>

              </p>
          </div>
          <hr />
          <div class="row gutter-xs">
               
              <div class="col-xs-12">
                   
                  <div class="card">

                      <div class="card-body">
                          <table id="demo-datatables-responsive-1" class="table table-bordered table-striped table-nowrap dataTable" cellspacing="0" width="100%" style="font-size: 12px">
                              <thead>
                              <tr>
                                  <th>ID</th>
                                  <th>Indicator Name</th>
                                 
                                  <th>KPI</th>
                                   <th>Relevance</th>
                                  <th>Weight (%)</th>
                                   <th><center>Status</center></th>
                                    <th><center>Actions</center></th>
                               


                              </tr>
                              </thead>
                              <tbody>
                              <tr>
                                  <td>2017/05/18</td>
                                  <td>gggggggggggggggggggggggggggggggggg</td>
                                  <td>10K</td>
                                   <td>10K</td>
                                   <td>10K</td>
                                   <td style="background-color:forestgreen;color:white"><center>  ACTIVE</center></td>
                                 

                               <td><Center> <button class="btn  btn-warning" style = "text-transform: capitalize; background-color: goldenrod; ; border-color: goldenrod"  type="button" data-toggle="modal" data-target="#editUser"> <i class = "icon icon-edit"></i></button> | <button class="btn  btn-warning" style = "text-transform: capitalize; background-color: black; ; border-color: black"  type="button" data-toggle="modal" data-target="#disUser"> <i class = "icon icon-lock"></i></button> |  <button class="btn  btn-warning" style = "text-transform: capitalize;  background-color: maroon; border-color: maroon"  type="button" data-toggle="modal" data-target="#delUser"> <i class = "icon icon-trash"></i></button></Center></td>


                              </tr>
                              <tr>
                                  <td>2017/05/18</td>
                                  <td>11:45:34 AM</td>
                                  <td>Madina</td>
                                <td>10K</td> <td>10K</td>
                              <td style="background-color:forestgreen;color:white"> <center> ACTIVE</center></tD>

                          <td><Center> <button class="btn  btn-warning" style = "text-transform: capitalize; background-color: goldenrod; ; border-color: goldenrod"  type="button" data-toggle="modal" data-target="#editUser"> <i class = "icon icon-edit"></i></button> | <button class="btn  btn-warning" style = "text-transform: capitalize; background-color: black; ; border-color: black"  type="button" data-toggle="modal" data-target="#disUser"> <i class = "icon icon-lock"></i></button> |  <button class="btn  btn-warning" style = "text-transform: capitalize;  background-color: maroon; border-color: maroon"  type="button" data-toggle="modal" data-target="#delUser"> <i class = "icon icon-trash"></i></button></Center></td>



                              </tr>
                              <tr>
                                  <td>2017/05/18</td>
                                  <td>11:45:34 AM</td>
                                  <td>10K</td>
                                   <td>10K</td>
                                   <td>10K</td>
                                   <td style="background-color:forestgreen;color:white"> <center> ACTIVE</center></tD>
                                  

                              <td><Center> <button class="btn  btn-warning" style = "text-transform: capitalize; background-color: goldenrod; ; border-color: goldenrod"  type="button" data-toggle="modal" data-target="#editUser"> <i class = "icon icon-edit"></i></button> | <button class="btn  btn-warning" style = "text-transform: capitalize; background-color: black; ; border-color: black"  type="button" data-toggle="modal" data-target="#disUser"> <i class = "icon icon-lock"></i></button> |  <button class="btn  btn-warning" style = "text-transform: capitalize;  background-color: maroon; border-color: maroon"  type="button" data-toggle="modal" data-target="#delUser"> <i class = "icon icon-trash"></i></button></Center></td>



                              </tr>
                              <tr>
                                  <td>2017/05/18</td>
                                  <td>11:45:34 AM</td>
                                  <td>10K</td>
                                   <td>10K</td>
                                   <td>10K</td>
                               <td style="background-color:maroon; color:white"><center> INACTIVE</center></td>
                                  

                          <td><Center> <button class="btn  btn-warning" style = "text-transform: capitalize; background-color: goldenrod; ; border-color: goldenrod"  type="button" data-toggle="modal" data-target="#editUser"> <i class = "icon icon-edit"></i></button> | <button class="btn  btn-warning" style = "text-transform: capitalize; background-color: black; ; border-color: black"  type="button" data-toggle="modal" data-target="#disUser"> <i class = "icon icon-lock"></i></button> |  <button class="btn  btn-warning" style = "text-transform: capitalize;  background-color: maroon; border-color: maroon"  type="button" data-toggle="modal" data-target="#delUser"> <i class = "icon icon-trash"></i></button></Center></td>


                              </tr>


                              </tbody>
                          </table>
                      </div>
                  </div>
              </div>

              
          </div>

       </div>




      <div id="logout" class="modal fade" tabindex="-1" role="dialog">
          <div class="modal-dialog">
              <div class="modal-content">
                  <div class="modal-header bg-primary" style="background-color: #217345">
                      <button type="button" class="close" data-dismiss="modal" style="color: #fff">
                          <span aria-hidden="true" style="color: #fff">×</span>
                          <span class="sr-only">Close</span>
                      </button>
                      <div class="text-center">
                          <span class="icon icon-lock icon-5x m-y-lg"></span>
                          <h4 class="modal-title" style="font-size: 14px">System Logout</h4>

                      </div>
                  </div>
                  <div class="modal-tabs">
                      <ul class="nav nav-tabs nav-justified">
                          <li class="active"><a href="#display" data-toggle="tab" style="font-size: 12px">Do You Want To Logout From LoyalStar?</a></li>

                      </ul>

                  </div>
                  <div class="modal-footer">
                      <center><button type="button" class="btn btn-primary" style="background-color: #217345"><i class="icon icon-check"></i>&nbsp &nbsp Yes &nbsp &nbsp</button> <button type="button" class="btn btn-danger" style="background-color: maroon"><i class="icon icon-close"></i> Cancel</button></center>

                  </div>
              </div>
          </div>
      </div>


      <div id="createSector" class="modal fade" tabindex="-1" role="dialog">
          <div class="modal-dialog">
              <div class="modal-content">
                  <div class="modal-header bg-primary" style="background-color: #001C98">
                      <button type="button" class="close" data-dismiss="modal" style="color: #fff" >
                          <span aria-hidden="true" style="color: #fff">×</span>
                          <span class="sr-only">Close</span>
                      </button>
                      <div class="text-center">
                          <span class="icon icon-plus icon-5x m-y-lg"></span>
                          <h4 class="modal-title" style="font-size: 12px">Create New KPI</h4>

                      </div>
                  </div>
                  <div class="modal-tabs">

                      <div class="tab-content">
                          <div class="tab-pane fade active in" id="display2">
                              <form action="/">
                                  <div class="form-group">
                                      <div class="row">
                                          <div class="col-md-12">
                                              <label  class="form-label" style="font-size: 12px">KPI Name</label>
                                              <input id="form-control-6" class="form-control" type="password" style="font-size: 11px"></div>

                                         

                                      </div></div>
                                  <div class="form-group">
                                      <div class="row">
                                          <div class="col-md-12">
                                              <label  class="form-label" style="font-size: 12px">Indicator</label>
                                              <select id="demo-select2-3" class="form-control" style="font-size: 11px">
                                                  <option value="administrator" >Male</option>
                                                  <option value="author">Sex</option>
                                                  <option value="contributor">Other</option>

                                              </select>
                                          </div>


                                          

                                      </div>
                                  </div>

                                           <div class="form-group">
                                      <div class="row">
                                          <div class="col-md-6">
                                              <label  class="form-label" style="font-size: 12px">Relevance</label>
                                              <input id="form-control-17" class="form-control" type="number" style="font-size: 11px">
                                          </div>

                                           <div class="col-md-6">
                                              <label  class="form-label" style="font-size: 12px">Weight (%)</label>
                                              <input id="form-control-137" class="form-control" type="number" style="font-size: 11px">
                                          </div>
                                          

                                      </div>
                                  </div>
                                  
                                  <hr style="border-color: #217345">

                                


                              </form>

                          </div>

                      </div>
                  </div>
                  <div class="modal-footer">

                      <button type="button" class="btn btn-primary" style="background-color: #217345"><i class="icon icon-save"></i> Save</button>
                  </div>
              </div>
          </div>
      </div>



      <div id="createBranch" class="modal fade" tabindex="-1" role="dialog">
          <div class="modal-dialog">
              <div class="modal-content">
                  <div class="modal-header bg-primary" style="background-color: #001C98">
                      <button type="button" class="close" data-dismiss="modal" style="color: #fff" >
                          <span aria-hidden="true" style="color: #fff">×</span>
                          <span class="sr-only">Close</span>
                      </button>
                      <div class="text-center">
                          <span class="icon icon-plus icon-5x m-y-lg"></span>
                          <h4 class="modal-title" style="font-size: 12px">Create A Branch</h4>

                      </div>
                  </div>
                  <div class="modal-tabs">

                      <div class="tab-content">
                          <div class="tab-pane fade active in" id="display">
                              <form action="/">
                                     <div class="row">
                                  <div class="form-group">
                                       <div class="col-md-6">
                                      <label  class="form-label" style="font-size: 12px">Branch ID</label>
                                      <input id="form-control-1" class="form-control" type="password" style="font-size: 11px">
</div>
                                      <div class="col-md-6">
                                              <label  class="form-label" style="font-size: 12px">Branch Name</label>
                                              <input id="form-control-2" class="form-control" type="password" style="font-size: 11px">

                                          </div>
                                  </div></div>
                                  <div class="row">
                                      <div class="form-group">
                                          <div class="col-md-6">
                                              <label  class="form-label" style="font-size: 12px">Sector</label>
                                               <select id="demo-select2-8" class="form-control" style="font-size: 11px">
                                                  <option value="administrator" >Male</option>
                                                  <option value="author">Sex</option>
                                                  <option value="contributor">Other</option>

                                              </select>
                                          </div>


                                          <div class="col-md-6">
                                              <label  class="form-label" style="font-size: 12px">Category</label>
                                               <select id="demo-select2-1" class="form-control" style="font-size: 11px">
                                                  <option value="administrator" >Large</option>
                                                  <option value="author">Medium</option>
                                                  <option value="contributor">Small</option>

                                              </select>

                                      </div></div></div>
                                     <div class="row">
                                   <div class="form-group">
      <div class="col-md-12">
                                              <label  class="form-label" style="font-size: 12px">Description</label>
                                              <input id="form-control-69" class="form-control" type="password" style="font-size: 11px">

                                          </div>
                                  </div></div>


                              </form>

                          </div>

                      </div>
                  </div>
                  <div class="modal-footer">

                      <button type="button" class="btn btn-primary" style="background-color: #217345"><i class="icon icon-save"></i> Save</button>
                  </div>
              </div>
          </div>
      </div>

      <div id="delUser" class="modal fade" tabindex="-1" role="dialog">
          <div class="modal-dialog">
              <div class="modal-content">
                  <div class="modal-header bg-primary" style="background-color: #217345">
                      <button type="button" class="close" data-dismiss="modal" style="color: #fff">
                          <span aria-hidden="true" style="color: #fff">×</span>
                          <span class="sr-only">Close</span>
                      </button>
                      <div class="text-center">
                          <span class="icon icon-trash icon-5x m-y-lg"></span>
                          <h4 class="modal-title" style="font-size: 14px">User Account Delete</h4>

                      </div>
                  </div>
                  <div class="modal-tabs">
                      <ul class="nav nav-tabs nav-justified">
                          <li class="active"><a href="#display" data-toggle="tab" style="font-size: 12px">Do You Want To Delete This User() From LoyalStar?</a></li>

                      </ul>

                  </div>
                  <div class="modal-footer">
                      <center><button type="button" class="btn btn-primary" style="background-color: #217345"><i class="icon icon-check"></i>&nbsp &nbsp Yes &nbsp &nbsp</button> <button type="button" class="btn btn-danger" style="background-color: maroon"><i class="icon icon-close"></i> Cancel</button></center>

                  </div>
              </div>
          </div>
      </div>


      <div id="disUser" class="modal fade" tabindex="-1" role="dialog">
          <div class="modal-dialog">
              <div class="modal-content">
                  <div class="modal-header bg-primary" style="background-color: #217345">
                      <button type="button" class="close" data-dismiss="modal" style="color: #fff">
                          <span aria-hidden="true" style="color: #fff">×</span>
                          <span class="sr-only">Close</span>
                      </button>
                      <div class="text-center">
                          <span class="icon icon-lock icon-5x m-y-lg"></span>
                          <h4 class="modal-title" style="font-size: 14px">Disable User Account</h4>

                      </div>
                  </div>
                  <div class="modal-tabs">
                      <ul class="nav nav-tabs nav-justified">
                          <li class="active"><a href="#display" data-toggle="tab" style="font-size: 12px">Do You Want To Disable This User() From LoyalStar?</a></li>

                      </ul>

                  </div>
                  <div class="modal-footer">
                      <center><button type="button" class="btn btn-primary" style="background-color: #217345"><i class="icon icon-check"></i>&nbsp &nbsp Yes &nbsp &nbsp</button> <button type="button" class="btn btn-danger" style="background-color: maroon"><i class="icon icon-close"></i> Cancel</button></center>

                  </div>
              </div>
          </div>
      </div>

      <div id="editUser" class="modal fade" tabindex="-1" role="dialog">
          <div class="modal-dialog">
              <div class="modal-content">
                  <div class="modal-header bg-primary" style="background-color: #217345">
                      <button type="button" class="close" data-dismiss="modal" style="color: #fff" >
                          <span aria-hidden="true" style="color: #fff">×</span>
                          <span class="sr-only">Close</span>
                      </button>
                      <div class="text-center">
                          <span class="icon icon-edit icon-5x m-y-lg"></span>
                          <h4 class="modal-title" style="font-size: 12px">Edit User's Details</h4>

                      </div>
                  </div>
                  <div class="modal-tabs">

                      <div class="tab-content">
                          <div class="tab-pane fade active in" id="display4">
                              <form action="/">
                                  <div class="form-group">
                                      <div class="row">
                                          <div class="col-md-12">
                                              <label  class="form-label" style="font-size: 12px">User's Full Name</label>
                                              <input id="form-control-9" class="form-control" type="password" style="font-size: 11px"></div>



                                      </div></div>
                                  <div class="form-group">
                                      <div class="row">
                                          <div class="col-md-6">
                                              <label  class="form-label" style="font-size: 12px">Sex</label>

                                              <select id="demo-select2-10" class="form-control" style="font-size: 11px">
                                                  <option value="administrator" >Male</option>
                                                  <option value="author">Sex</option>
                                                  <option value="contributor">Other</option>

                                              </select>
                                          </div>


                                          <div class="col-md-6">
                                              <label  class="form-label" style="font-size: 12px">Phone Number</label>
                                              <input id="form-control-11" class="form-control" type="password" style="font-size: 11px">
                                          </div>

                                      </div>
                                  </div><hr style="border-color: #217345">

                                  <div class="form-group">
                                      <div class="row">
                                          <div class="col-md-12">
                                              <label  class="form-label" style="font-size: 12px">User Role</label>
                                              <select id="demo-select2-9" class="form-control" style="font-size: 11px">
                                                  <option value="administrator" >Male</option>
                                                  <option value="author">Sex</option>
                                                  <option value="contributor">Other</option>

                                              </select>
                                          </div>



                                      </div>
                                  </div>


                              </form>

                          </div>

                      </div>
                  </div>
                  <div class="modal-footer">

                      <button type="button" class="btn btn-primary" style="background-color: #217345"><i class="icon icon-save"></i> Save</button>
                  </div>
              </div>
          </div>
      </div>





    </div>
    <div id="rates" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <button type="button" class="close" data-dismiss="modal" style="color: #fff">
                        <span aria-hidden="true" style="color: #fff">×</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <div class="text-center">
                        <span class="icon icon-bar-chart icon-5x m-y-lg"></span>
                        <h4 class="modal-title" style="font-size: 12px">Exchange Rates</h4>

                    </div>
                </div>
                <div class="modal-tabs">

                    <div class="tab-content">
                        <div class="tab-pane fade active in" id="displayi">
                        <div class = "row">
                        <div class = "col-lg-12">

                            <div class = "row">
                                <div class="col-xs-6 col-md-6">
                                    <div class="card" style  = "background-color:#600076; color: #fff ">
                                        <div class="card-values">
                                            <div class="p-x">
                                                <small>Ghana <img src = "assets/img/gh.png" height="20px" width="20px"></small>
                                                <h5 class="card-title fw-l" style="margin-top: 20px">1.00 USD = 4.00 GHS</h5>
                                            </div>
                                        </div>
                                        <div class="card-chart">
                                            <canvas data-chart="line" data-animation="false" data-labels='["Jun 21", "Jun 20", "Jun 19", "Jun 18", "Jun 17", "Jun 16", "Jun 15"]' data-values='[{"backgroundColor": "rgba(39, 174, 96, 0.03)", "borderColor": "#600076", "data": [25250, 23370, 25568, 28961, 26762, 30072, 25135]}]' data-scales='{"yAxes": [{ "ticks": {"max": 32327}}]}' data-hide='["legend", "points", "scalesX", "scalesY", "tooltips"]' height="35"></canvas>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-md-6">
                                    <div class="card" style  = "background-color:#600076; color: #fff ">
                                        <div class="card-values">
                                            <div class="p-x">
                                                <small>Nigeria <img src = "assets/img/ng.png" height="20px" width="20px"></small>
                                                <h5 class="card-title fw-l" style="margin-top: 20px">1.00 USD = 4.00 GHS</h5>
                                            </div>
                                        </div>
                                        <div class="card-chart">
                                            <canvas data-chart="line" data-animation="false" data-labels='["Jun 21", "Jun 20", "Jun 19", "Jun 18", "Jun 17", "Jun 16", "Jun 15"]' data-values='[{"backgroundColor": "rgba(39, 174, 96, 0.03)", "borderColor": "#600076", "data": [25250, 23370, 25568, 28961, 26762, 30072, 25135]}]' data-scales='{"yAxes": [{ "ticks": {"max": 32327}}]}' data-hide='["legend", "points", "scalesX", "scalesY", "tooltips"]' height="35"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        </div>

                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>




    <script src="contents/js/vendor.min.js"></script>
    <script src="contents/js/elephant.min.js"></script>
    <script src="contents/js/application.min.js"></script>
    <script src="contents/js/demo.min.js"></script>
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
      ga('create', 'UA-83990101-1', 'auto');
      ga('send', 'pageview');
    </script>
</div></body>
</html>

