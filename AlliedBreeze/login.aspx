﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="AlliedBreeze.login" %>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Log In &middot; Allied Breeze | Appraisal System</title>
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no">
    <meta name="description" content="Elephant is a front-end template created to help you build modern web applications, fast and in a professional manner.">
    <meta property="og:url" content="http://demo.naksoid.com/elephant">
    <meta property="og:type" content="website">
    <meta property="og:title" content="The fastest way to build modern admin site for any platform, browser, or device">
    <meta property="og:description" content="Elephant is a front-end template created to help you build modern web applications, fast and in a professional manner.">
    <meta property="og:image" content="http://demo.naksoid.com/elephant/img/ae165ef33d137d3f18b7707466aa774d.jpg">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="@naksoid">
    <meta name="twitter:creator" content="@naksoid">
    <meta name="twitter:title" content="The fastest way to build modern admin site for any platform, browser, or device">
    <meta name="twitter:description" content="Elephant is a front-end template created to help you build modern web applications, fast and in a professional manner.">
    <meta name="twitter:image" content="http://demo.naksoid.com/elephant/img/ae165ef33d137d3f18b7707466aa774d.jpg">
    <link rel="apple-touch-icon" sizes="180x180" href="apple-touch-icon.png">
    <link rel="icon" type="image/png" href="favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="manifest.json">
    <link rel="mask-icon" href="safari-pinned-tab.svg" color="#001C98">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,400italic,500,700">
    <link rel="stylesheet" href="contents/css/vendor.min.css">
    <link rel="stylesheet" href="contents/css/elephant.min.css">
    <link rel="stylesheet" href="contents/css/login-2.min.css">
  </head>
  <body style ="background-color:#001C98">
    <div class="login" style ="background-color:#fff; height:420px">
      <div class="login-body" >
        <a class="login-brand" href="login.aspx" style="margin-left:-1px; width:180px; height:15px">
          <img class="img-responsive" src="contents/img/AlliedBreeze.png" alt="Elephant">
        </a>
          <br />
          <h3 class="login-heading" style ="font-size:1.8em; color:#001C98; font-family:Verdana, Tahoma, Geneva, Verdana, sans-serif; font-weight:100">Sign In</h3>
        <div class="login-form">

             <form data-toggle="md-validator">
                 
            <div class="md-form-group md-label-floating">
              <input class="md-form-control" type="text" name="staffid" spellcheck="false" autocomplete="off" data-msg-required="Please enter your Staff ID." required runat="server" id ="staffid">
              <label class="md-control-label">Staff ID</label>
            </div>
            <div class="md-form-group md-label-floating">
              <input class="md-form-control" type="password" name="password" minlength="6" data-msg-minlength="Password must be 6 characters or more." data-msg-required="Please enter your password." required runat="server" id ="passwd">
              <label class="md-control-label">Password</label>
            </div>
            <div class="md-form-group md-custom-controls">
              <label class="custom-control custom-control-primary custom-checkbox">
                <input class="custom-control-input" type="checkbox" checked="checked">
                <span class="custom-control-indicator"></span>
                <span class="custom-control-label">Keep me signed in</span>
              </label>
              <span aria-hidden="true"> · </span>
              <a href="resetpassword.aspx">Forgot password?</a>
            </div>
            <button class="btn btn-primary btn-block" type="submit" id="btnSubmit" runat="server" onserverclick="Submit_Click">Sign in</button>
          </form>
        </div>
      </div>
    <div class="login-footer" style ="color:#fff">
        Powered by B.I Development Team
      </div>
    </div>
    <script src="contents/js/vendor.min.js"></script>
    <script src="contents/js/elephant.min.js"></script>
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
      ga('create', 'UA-83990101-1', 'auto');
      ga('send', 'pageview');
    </script>
  </body>
</html>
