﻿<%@ Page Title="" Language="C#" MasterPageFile="~/alliedbreeze.Master" AutoEventWireup="true" CodeBehind="home.aspx.cs" Inherits="AlliedBreeze.home1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="layout-sidebar" >
        <div class="layout-sidebar-backdrop" style="background-color:white"></div>
        <div class="layout-sidebar-body" style="background-color:white">
          <div class="custom-scrollbar">
            <nav id="sidenav" class="sidenav-collapse collapse" style="background-color:white">
              <ul class="sidenav">
                  <br />
                <li class="sidenav-heading">General Settings</li>
                <li class="sidenav-item">
                  <a href="dashboard-1.html" aria-haspopup="true">
                    <span class="sidenav-icon icon icon-users"></span>
                    <span class="sidenav-label" style="font-size: 11px">Staff Management</span>
                  </a>
                </li>
                  <li class="sidenav-item">
                      <a href="dashboard-1.html" aria-haspopup="true">
                          <span class="sidenav-icon icon icon-briefcase"></span>
                          <span class="sidenav-label" style="font-size: 11px">Job Roles</span>
                      </a>
                  </li>
                   <li class="sidenav-item  ">
                      <a href="dashboard-1.html" aria-haspopup="true">
                          <span class="sidenav-icon icon icon-circle"></span>
                          <span class="sidenav-label" style="font-size: 11px">Parent Indicators</span>
                      </a>
                  </li>
                   <li class="sidenav-item  ">
                      <a href="dashboard-1.html" aria-haspopup="true">
                          <span class="sidenav-icon icon icon-chevron-down"></span>
                          <span class="sidenav-label" style="font-size: 11px">Child Indicators</span>
                      </a>
                  </li>
                    <li class="sidenav-item  ">
                      <a href="dashboard-1.html" aria-haspopup="true">
                          <span class="sidenav-icon icon  icon-map"></span>
                          <span class="sidenav-label" style="font-size: 11px">Sectorial &amp Branch Management</span>
                      </a>
                  </li>

                <li class="sidenav-heading">Data Input Management</li>
                <li class="sidenav-item ">
                  <a href="#" aria-haspopup="true">
                    <span class="sidenav-icon icon icon-contao"></span>
                    <span class="sidenav-label" style="font-size: 11px">Risk & Controls</span>
                  </a>

                </li>
                <li class="sidenav-item ">
                  <a href="#" aria-haspopup="true">
                    <span class="sidenav-icon icon icon-users"></span>
                    <span class="sidenav-label" style="font-size: 11px">Customer Service</span>
                  </a>

                </li>

                   <li class="sidenav-item ">
                  <a href="#" aria-haspopup="true">
                    <span class="sidenav-icon icon icon-thumbs-up"></span>
                    <span class="sidenav-label" style="font-size: 11px">Qualitative Objectives</span>
                  </a>

                </li>

                  <li class="sidenav-heading">Reporting Portals</li>
                  <li class="sidenav-item ">
                      <a href="#" aria-haspopup="true">
                          <span class="sidenav-icon icon icon-bell-o"></span>
                          <span class="sidenav-label" style="font-size: 11px">Individual</span>
                      </a>

                  </li>
                  <li class="sidenav-item ">
                      <a href="#" aria-haspopup="true">
                          <span class="sidenav-icon icon icon-list"></span>
                          <span class="sidenav-label" style="font-size: 11px">Branchwise</span>
                      </a>

                  </li>

                    <li class="sidenav-item ">
                      <a href="#" aria-haspopup="true">
                          <span class="sidenav-icon icon icon-list"></span>
                          <span class="sidenav-label" style="font-size: 11px">Departmental</span>
                      </a>

                  </li>

                    <li class="sidenav-item ">
                      <a href="#" aria-haspopup="true">
                          <span class="sidenav-icon icon icon-list"></span>
                          <span class="sidenav-label" style="font-size: 11px">Sectorial</span>
                      </a>

                  </li>

                    <li class="sidenav-item ">
                      <a href="#" aria-haspopup="true">
                          <span class="sidenav-icon icon icon-list"></span>
                          <span class="sidenav-label" style="font-size: 11px">Executive</span>
                      </a>

                  </li>



              </ul>
            </nav>
          </div>
        </div>
      </div>
    <div class="layout-content">
        <div class="layout-content-body">
            <br />
            <div class="title-bar">

              <h1 class="title-bar-title">
                  <span class="d-ib pull-left">Home &nbsp &nbsp </span>
                  <%-- <button class="pull-left btn btn-pill btn-success" style = "text-transform: capitalize; background-color: #001C98; border-color: #001C98 " data-toggle="modal" data-target="#createSector" type="button"> <i class = "icon icon-plus"></i> &nbsp New Indicator</button>--%>

              </h1>
              <p class="title-bar-description">
                  <small  style ="color:#14692E">About AlliedBreeze</small>

              </p>
          </div>
          <hr />
            <div class ="row gutter-xs" >
                <div class ="col-xs-12">
                    <div class ="card">
                        <p  style =" margin-left:25px; margin-top:15px; font-size:1.6em; color:#001C98"; font-family:Segoe UI, Verdana, Tahoma, Arial, Helvetica Neue, Helvetica, Sans-Serif; >Welcome to <span style ="color:#ad8800">AlliedBreeze</span> - FASL Appraisal System</p>
                        <table class ="tableCellText1" style ="margin:15px 27px;">
                            <tr >
                                <td style ="color:#655757;" >
                                    AlliedBreeze is a Key Performance Appraisal System. This appraisal system is categorised into two main sections; Quantitative and Qualitative.<br /> The Quantitative carries 80% of the staff’s performance and the qualitative 20%.<br /> 
The Quantitative has five measures including; The Financial Objective; Business Growth Objective; Customer Service Objective; People Element and Risk and Controls. Each of these quantitative objectives are weighted based on the staff’s core activity.<br /> 
The qualitative also measures staff’s appreciation and adherence to the organization’s core Value including; Speed; Diligence; Passion; Professionalism; Excellence; and Ethics. 
The overall performance of the staff is categorized as follows;
                                </td>
                            </tr>
                        </table>
                        <table class ="tableCellText1" style =" margin-left:80px; margin-top:15px; font-size:1.2em; color:#655757"; font-family:Segoe UI, Verdana, Tahoma, Arial, Helvetica Neue, Helvetica, Sans-Serif;>
                            <tr>
                                <td class ="pcmCellText">
                                    Unacceptable 
                                </td>
                                <td class ="tableCellText1" style ="color:#ff0000;">
                                    Below 50% of Targets
                                </td>
                                <td class ="pcmCellText">
                                    <i class ="fa fa-star fa_custom fa-spin" style ="color:#ff0000;"></i>
                                    <i class ="far fa-star fa_custom"></i>
                                    <i class ="far fa-star fa_custom"></i>
                                    <i class ="far fa-star fa_custom"></i>
                                    <i class ="far fa-star fa_custom"></i>
                                </td>
                            </tr>
                            <tr>
                                <td class ="pcmCellText" >
                                    Improvement Required
                                </td>
                                <td class ="tableCellText1" style ="color:#dbbc01;">
                                    50% – 75% of Targets
                                </td>
                                <td class ="pcmCellText">
                                    <i class ="fa fa-star fa_custom" style ="color:#dbbc01;"></i>
                                    <i class ="fa fa-star fa_custom fa-spin" style ="color:#dbbc01;"></i>
                                    <i class ="far fa-star fa_custom"></i>
                                    <i class ="far fa-star fa_custom"></i>
                                    <i class ="far fa-star fa_custom"></i>
                                </td>
                            </tr>
                            <tr >
                                <td class ="pcmCellText">
                                    Good Performance
                                </td>
                                <td class ="tableCellText1" style ="color:#04d03a">
                                     75% – 90% of Targets 
                                </td>
                                <td class ="pcmCellText">
                                    <i class ="fa fa-star fa_custom" style ="color:#04d03a;"></i>
                                    <i class ="fa fa-star fa_custom" style ="color:#04d03a;"></i>
                                    <i class ="fa fa-star fa_custom fa-spin" style ="color:#04d03a;"></i>
                                    <i class ="far fa-star fa_custom"></i>
                                    <i class ="far fa-star fa_custom"></i>
                                </td>
                            </tr>
                            <tr>
                                <td class ="pcmCellText">
                                    Very Good Performance 
                                </td>
                                <td class ="tableCellText1"  style ="color:#00921e">
                                     90% – 100% of Targets
                                </td>
                                <td>
                                    <i class ="fa fa-star fa_custom" style ="color:#00921e;"></i>
                                    <i class ="fa fa-star fa_custom" style ="color:#00921e;"></i>
                                    <i class ="fa fa-star fa_custom" style ="color:#00921e;"></i>
                                    <i class ="fa fa-star fa_custom fa-spin" style ="color:#00921e;"></i>
                                    <i class ="far fa-star fa_custom"></i>
                                </td>
                            </tr>
                            <tr>
                                <td class ="pcmCellText">
                                    Exceptional Performance
                                </td>
                                <td class ="tableCellText1"  style ="color:#00921e">
                                     100% and Above of Targets
                                </td>
                                <td class ="pcmCellText">
                                    <i class ="fa fa-star fa_custom" style ="color:#00921e;"></i>
                                    <i class ="fa fa-star fa_custom" style ="color:#00921e;"></i>
                                    <i class ="fa fa-star fa_custom" style ="color:#00921e;"></i>
                                    <i class ="fa fa-star fa_custom" style ="color:#00921e;"></i>
                                    <i class ="fa fa-star fa_custom fa-spin" style ="color:#00921e;"></i>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                
            </div>
           
       

        </div>
      </div>
</asp:Content>
