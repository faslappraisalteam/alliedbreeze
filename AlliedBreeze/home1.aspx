﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="home1.aspx.cs" Inherits="AlliedBreeze.home" %>


<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>AlliedBreeze</title>
    <meta name="viewport" content="">
    <meta name="description" content="">
    <meta property="og:url" content="">
    <meta property="og:type" content="">
    <meta property="og:title" content="">
    <meta property="og:description" content="">
    <meta property="og:image" content="">
    <meta name="twitter:card" content="">
    <meta name="twitter:site" content="">
    <meta name="twitter:creator" content="">
    <meta name="twitter:title" content="">
    <meta name="twitter:description" content="">
    <meta name="twitter:image" content="">
    <link rel="apple-touch-icon" sizes="180x180" href="">
    <link rel="icon" type="image/png" href="" sizes="32x32">
    <link rel="icon" type="image/png" href="" sizes="16x16">
    <link rel="manifest" href="manifest.json">
    <link rel="mask-icon" href="safari-pinned-tab.svg" color="#001C98">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,400italic,500,700">
    <link rel="stylesheet" href="../contents/css/vendor.min.css">
    <link rel="stylesheet" href="../contents/css/elephant.min.css">
    <link rel="stylesheet" href="../contents/css/application.min.css">
    <link rel="stylesheet" href="../contents/css/demo.min.css">
    <script defer src="../contents/js/fontawesome-all.js"></script>
  </head>
  <body class="layout layout-header-fixed">
    <div class="layout-header">
      <div class="navbar navbar-default" style = "background-color: #001C98; height:75px">
        <div class="navbar-header" style = "background-color: #001C98">
          <a class="navbar-brand navbar-brand-center" href="audience.html">
            <img class="navbar-brand-logo" src="contents/img/loo.png" alt="" style = "margin-top: -15px; height:72px; width:76px">
          </a>
          <button class="navbar-toggler visible-xs-block collapsed" type="button" data-toggle="collapse" data-target="#sidenav">
            <span class="sr-only">Toggle navigation</span>
            <span class="bars">
              <span class="bar-line bar-line-1 out"></span>
              <span class="bar-line bar-line-2 out"></span>
              <span class="bar-line bar-line-3 out"></span>
            </span>
            <span class="bars bars-x">
              <span class="bar-line bar-line-4"></span>
              <span class="bar-line bar-line-5"></span>
            </span>
          </button>
          <button class="navbar-toggler visible-xs-block collapsed" type="button" data-toggle="collapse" data-target="#navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="arrow-up"></span>
            <span class="ellipsis ellipsis-vertical">
              <img class="ellipsis-object" width="32" height="32" src="contents/img/user.png" alt="Teddy Wilson">
            </span>
          </button>
        </div>
        <div class="navbar-toggleable">
          <nav id="navbar" class="navbar-collapse collapse">
            <button class="sidenav-toggler hidden-xs" title="Collapse sidenav ( [ )" aria-expanded="true" type="button">
              <span class="sr-only">Toggle navigation</span>
              <span class="bars">
                <span class="bar-line bar-line-1 out"></span>
                <span class="bar-line bar-line-2 out"></span>
                <span class="bar-line bar-line-3 out"></span>
                <span class="bar-line bar-line-4 in"></span>
                <span class="bar-line bar-line-5 in"></span>
                <span class="bar-line bar-line-6 in"></span>
              </span>
            </button>
            <ul class="nav navbar-nav navbar-right">
              <li class="visible-xs-block">
                <h4 class="navbar-text text-center">Hi, Ama Mantebea</h4>
              </li>


              <li class="dropdown hidden-xs">
                <button class="navbar-account-btn" data-toggle="dropdown" aria-haspopup="true" style="margin-top:15px">
                  <img class="rounded" width="36" height="36" src="contents/img/user.png" alt=""> &nbsp &nbsp Ama Mantebea | <a href="login-1.html"><span style = "color: #fff"><i class = "icon icon-lock"> </i> Sign out</span></a>

                </button>


              </li>


              <li class="visible-xs-block">
                <a href="login-1.html">
                  <span class="icon icon-power-off icon-lg icon-fw"></span>
                  Sign out
                </a>
              </li>
            </ul>
          </nav>
        </div>
      </div>
    </div>

    <div class="layout-main" >
      <div class="layout-sidebar" >
        <div class="layout-sidebar-backdrop" style="background-color:white"></div>
        <div class="layout-sidebar-body" style="background-color:white">
          <div class="custom-scrollbar">
            <nav id="sidenav" class="sidenav-collapse collapse" style="background-color:white">
              <ul class="sidenav">
                  <br />
                <li class="sidenav-heading">General Settings</li>
                <li class="sidenav-item  active">
                  <a href="dashboard-1.html" aria-haspopup="true">
                    <span class="sidenav-icon icon icon-users"></span>
                    <span class="sidenav-label" style="font-size: 11px">Staff Management</span>
                  </a>
                </li>
                  <li class="sidenav-item  ">
                      <a href="dashboard-1.html" aria-haspopup="true">
                          <span class="sidenav-icon icon icon-briefcase"></span>
                          <span class="sidenav-label" style="font-size: 11px">Job Roles</span>
                      </a>
                  </li>
                   <li class="sidenav-item  ">
                      <a href="dashboard-1.html" aria-haspopup="true">
                          <span class="sidenav-icon icon icon-circle"></span>
                          <span class="sidenav-label" style="font-size: 11px">Parent Indicators</span>
                      </a>
                  </li>
                   <li class="sidenav-item  ">
                      <a href="dashboard-1.html" aria-haspopup="true">
                          <span class="sidenav-icon icon icon-chevron-down"></span>
                          <span class="sidenav-label" style="font-size: 11px">Child Indicators</span>
                      </a>
                  </li>
                    <li class="sidenav-item  ">
                      <a href="dashboard-1.html" aria-haspopup="true">
                          <span class="sidenav-icon icon  icon-map"></span>
                          <span class="sidenav-label" style="font-size: 11px">Sectorial &amp Branch Management</span>
                      </a>
                  </li>

                <li class="sidenav-heading">Data Input Management</li>
                <li class="sidenav-item ">
                  <a href="#" aria-haspopup="true">
                    <span class="sidenav-icon icon icon-contao"></span>
                    <span class="sidenav-label" style="font-size: 11px">Risk & Controls</span>
                  </a>

                </li>
                <li class="sidenav-item ">
                  <a href="#" aria-haspopup="true">
                    <span class="sidenav-icon icon icon-users"></span>
                    <span class="sidenav-label" style="font-size: 11px">Customer Service</span>
                  </a>

                </li>

                   <li class="sidenav-item ">
                  <a href="#" aria-haspopup="true">
                    <span class="sidenav-icon icon icon-thumbs-up"></span>
                    <span class="sidenav-label" style="font-size: 11px">Qualitative Objectives</span>
                  </a>

                </li>

                  <li class="sidenav-heading">Reporting Portals</li>
                  <li class="sidenav-item ">
                      <a href="#" aria-haspopup="true">
                          <span class="sidenav-icon icon icon-bell-o"></span>
                          <span class="sidenav-label" style="font-size: 11px">Individual</span>
                      </a>

                  </li>
                  <li class="sidenav-item ">
                      <a href="#" aria-haspopup="true">
                          <span class="sidenav-icon icon icon-list"></span>
                          <span class="sidenav-label" style="font-size: 11px">Branchwise</span>
                      </a>

                  </li>

                    <li class="sidenav-item ">
                      <a href="#" aria-haspopup="true">
                          <span class="sidenav-icon icon icon-list"></span>
                          <span class="sidenav-label" style="font-size: 11px">Departmental</span>
                      </a>

                  </li>

                    <li class="sidenav-item ">
                      <a href="#" aria-haspopup="true">
                          <span class="sidenav-icon icon icon-list"></span>
                          <span class="sidenav-label" style="font-size: 11px">Sectorial</span>
                      </a>

                  </li>

                    <li class="sidenav-item ">
                      <a href="#" aria-haspopup="true">
                          <span class="sidenav-icon icon icon-list"></span>
                          <span class="sidenav-label" style="font-size: 11px">Executive</span>
                      </a>

                  </li>



              </ul>
            </nav>
          </div>
        </div>
      </div>
      <div class="layout-content">
        <div class="layout-content-body">
            <br />
            <div class="title-bar">

              <h1 class="title-bar-title">
                  <span class="d-ib pull-left">Home &nbsp &nbsp </span>
                  <%-- <button class="pull-left btn btn-pill btn-success" style = "text-transform: capitalize; background-color: #001C98; border-color: #001C98 " data-toggle="modal" data-target="#createSector" type="button"> <i class = "icon icon-plus"></i> &nbsp New Indicator</button>--%>

              </h1>
              <p class="title-bar-description">
                  <small  style ="color:#14692E">About AlliedBreeze</small>

              </p>
          </div>
          <hr />
            <div class ="row gutter-xs" >
                <div class ="col-xs-12">
                    <div class ="card">
                        <p  style =" margin-left:25px; margin-top:15px; font-size:1.6em; color:#001C98"; font-family:Segoe UI, Verdana, Tahoma, Arial, Helvetica Neue, Helvetica, Sans-Serif; >Welcome to <span style ="color:#ad8800">AlliedBreeze</span> - FASL Appraisal System</p>
                        <table class ="tableCellText1" style ="margin:15px 27px;">
                            <tr >
                                <td style ="color:#655757;" >
                                    AlliedBreeze is a Key Performance Appraisal System. This appraisal system is categorised into two main sections; Quantitative and Qualitative.<br /> The Quantitative carries 80% of the staff’s performance and the qualitative 20%.<br /> 
The Quantitative has five measures including; The Financial Objective; Business Growth Objective; Customer Service Objective; People Element and Risk and Controls. Each of these quantitative objectives are weighted based on the staff’s core activity.<br /> 
The qualitative also measures staff’s appreciation and adherence to the organization’s core Value including; Speed; Diligence; Passion; Professionalism; Excellence; and Ethics. 
The overall performance of the staff is categorized as follows;
                                </td>
                            </tr>
                        </table>
                        <table class ="tableCellText1" style =" margin-left:80px; margin-top:15px; font-size:1.2em; color:#655757"; font-family:Segoe UI, Verdana, Tahoma, Arial, Helvetica Neue, Helvetica, Sans-Serif;>
                            <tr>
                                <td class ="pcmCellText">
                                    Unacceptable 
                                </td>
                                <td class ="tableCellText1" style ="color:#ff0000;">
                                    Below 50% of Targets
                                </td>
                                <td class ="pcmCellText">
                                    <i class ="fa fa-star fa_custom fa-spin" style ="color:#ff0000;"></i>
                                    <i class ="far fa-star fa_custom"></i>
                                    <i class ="far fa-star fa_custom"></i>
                                    <i class ="far fa-star fa_custom"></i>
                                    <i class ="far fa-star fa_custom"></i>
                                </td>
                            </tr>
                            <tr>
                                <td class ="pcmCellText" >
                                    Improvement Required
                                </td>
                                <td class ="tableCellText1" style ="color:#dbbc01;">
                                    50% – 75% of Targets
                                </td>
                                <td class ="pcmCellText">
                                    <i class ="fa fa-star fa_custom" style ="color:#dbbc01;"></i>
                                    <i class ="fa fa-star fa_custom fa-spin" style ="color:#dbbc01;"></i>
                                    <i class ="far fa-star fa_custom"></i>
                                    <i class ="far fa-star fa_custom"></i>
                                    <i class ="far fa-star fa_custom"></i>
                                </td>
                            </tr>
                            <tr >
                                <td class ="pcmCellText">
                                    Good Performance
                                </td>
                                <td class ="tableCellText1" style ="color:#04d03a">
                                     75% – 90% of Targets 
                                </td>
                                <td class ="pcmCellText">
                                    <i class ="fa fa-star fa_custom" style ="color:#04d03a;"></i>
                                    <i class ="fa fa-star fa_custom" style ="color:#04d03a;"></i>
                                    <i class ="fa fa-star fa_custom fa-spin" style ="color:#04d03a;"></i>
                                    <i class ="far fa-star fa_custom"></i>
                                    <i class ="far fa-star fa_custom"></i>
                                </td>
                            </tr>
                            <tr>
                                <td class ="pcmCellText">
                                    Very Good Performance 
                                </td>
                                <td class ="tableCellText1"  style ="color:#00921e">
                                     90% – 100% of Targets
                                </td>
                                <td>
                                    <i class ="fa fa-star fa_custom" style ="color:#00921e;"></i>
                                    <i class ="fa fa-star fa_custom" style ="color:#00921e;"></i>
                                    <i class ="fa fa-star fa_custom" style ="color:#00921e;"></i>
                                    <i class ="fa fa-star fa_custom fa-spin" style ="color:#00921e;"></i>
                                    <i class ="far fa-star fa_custom"></i>
                                </td>
                            </tr>
                            <tr>
                                <td class ="pcmCellText">
                                    Exceptional Performance
                                </td>
                                <td class ="tableCellText1"  style ="color:#00921e">
                                     100% and Above of Targets
                                </td>
                                <td class ="pcmCellText">
                                    <i class ="fa fa-star fa_custom" style ="color:#00921e;"></i>
                                    <i class ="fa fa-star fa_custom" style ="color:#00921e;"></i>
                                    <i class ="fa fa-star fa_custom" style ="color:#00921e;"></i>
                                    <i class ="fa fa-star fa_custom" style ="color:#00921e;"></i>
                                    <i class ="fa fa-star fa_custom fa-spin" style ="color:#00921e;"></i>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                
            </div>
           
       

        </div>
      </div>

    </div>
  


    <script src="../contents/js/vendor.min.js"></script>
    <script src="../contents/js/elephant.min.js"></script>
    <script src="../contents/js/application.min.js"></script>
    <script src="../contents/js/demo.min.js"></script>
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
      ga('create', 'UA-83990101-1', 'auto');
      ga('send', 'pageview');
    </script>
  </body>
</html>
