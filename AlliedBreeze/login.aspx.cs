﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Configuration;
using System.Data.SqlClient;
using System.Data;
using AlliedBreeze.code;

namespace AlliedBreeze
{
    public partial class login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["out"] != null)
            {
                Session.RemoveAll();
                Session.Clear();
                Session.Abandon();

                if (Page.IsCallback)
                {
                    /*FormsAuthentication.SignOut()*/;
                    Response.RedirectLocation = "login.aspx";
                }
                if (!Page.IsCallback)
                {
                    //FormsAuthentication.SignOut();
                    Response.Redirect("login.aspx");
                }
            }
        }
        protected void Submit_Click(object sender, EventArgs e)
        {
            string myStaffId = staffid.Value;
            string myPasswd = passwd.Value;
            int passwordExpiresDays = 30;

            using (SqlConnection constr = new SqlConnection(WebConfigurationManager.ConnectionStrings["DBConStr"].ConnectionString))
            {

                DataRow oSystemUserRow = SqlHelper.ExecuteDataset(constr, CommandType.Text, string.Format("SELECT * FROM [dbo].[DimStaff] WHERE StaffID ='{0}'", myStaffId), null).Tables[0].Rows[0];
                if ((oSystemUserRow["Password"].ToString().Equals(myPasswd)) && (oSystemUserRow["StatusInd"].ToString().Equals("A")) && (oSystemUserRow["LastPasswordChange"].ToString() != oSystemUserRow["CreatedOn"].ToString()) && (Convert.ToDateTime(oSystemUserRow["LastPasswordChange"].ToString()).AddDays(passwordExpiresDays) > DateTime.Now))
                {
                    if (oSystemUserRow["JobRoleID"].Equals(150))
                    {
                        Response.Redirect("");
                        Session["LoggedInUser"] = myStaffId;
                        Session["SystemUserRow"] = oSystemUserRow;
                    }
                    else if (oSystemUserRow["JobRoleID"].Equals(200))
                    {
                        Response.Redirect("");
                        Session["LoggedInUser"] = myStaffId;
                        Session["SystemUserRow"] = oSystemUserRow;
                    }
                }

                else if (oSystemUserRow["StatusInd"].ToString().Equals("D"))
                {
                    ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "ClientScript", "alert('Your account is DISABLED. contact I.T support')", true);
                    /*FailureTextASPxLabel.Text = "Your Account is disabled. Please contact I.T Support for further assistance.";*/
                }
                else if (oSystemUserRow["StatusInd"].Equals("B"))
                {
                    ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "ClientScript", "alert('Your account is BLOCKED. contact I.T support')", true);
                    /*FailureTextASPxLabel.Text = "Your Account is blocked. Please contact I.T for further assistance."*/
                }
                else
                {
                    if (Session["CountFailed"] == null)
                    {
                        Session["CountFailed"] = 1;
                    }
                    else
                    {
                        Session["CountFailed"] = (int)Session["CountFailed"] + 1;
                    }

                    if (Session["CountFailed"].Equals(3))
                    {
                        ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "ClientScript", "alert('Your login has been temporarily disabled after several unsuccesful attempts. Contact I.T Support.')", true);

                        //FailureTextASPxLabel.Text = "Your login has been temporarily disabled after several unsuccesful attempts. Contact I.T Support.";
                    }
                    else
                    {
                        string FailureTextASPxLabel = string.Format("You have {0} failed login attempt. Your account will be locked after a 3rd failed login attempt", (int)Session["CountFailed"]);

                        ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "ClientScript", "alert(FailureTextASPxLabel)", true);

                    }
                }
            }



        }
    }
}