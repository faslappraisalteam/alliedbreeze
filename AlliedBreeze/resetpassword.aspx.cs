﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Net;
using System.Net.Mail;
using AlliedBreeze.code;



namespace AlliedBreeze
{
    public partial class resetpassword : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void Submit_Click(object sender, EventArgs e)
        {
            string mypassword, myEmail, myFullName = string.Empty;
            string myStaffId = staffid.Value;
            using (SqlConnection constr = new SqlConnection(WebConfigurationManager.ConnectionStrings["DBConStr"].ConnectionString))
            {
                DataTable dt = new DataTable();
                DataRow oSystemUserRow = SqlHelper.ExecuteDataset(constr, CommandType.Text, string.Format("SELECT * FROM [dbo].[DimStaff] WHERE StaffID ='{0}'", myStaffId), null).Tables[0].Rows[0];
                dt.Rows.Add(oSystemUserRow);
                myEmail =dt.Rows[0]["Email"].ToString();
                myFullName = dt.Rows[0]["StaffName"].ToString();
                mypassword = dt.Rows[0]["Password"].ToString();
                try
                {
                    var fromEmailAddress = WebConfigurationManager.AppSettings["FromEmailAddress"].ToString();
                    var fromEmailDisplayName = WebConfigurationManager.AppSettings["FromEmailDisplayName"].ToString();
                    var fromEmailPassword = WebConfigurationManager.AppSettings["FromEmailPassword"].ToString();
                    var smtpHost = WebConfigurationManager.AppSettings["SMTPHost"].ToString();
                    var smtpPort = WebConfigurationManager.AppSettings["SMTPPort"].ToString();

                    string body = "Your password is: " + Server.HtmlEncode(mypassword) + " Thank you.";

                    MailMessage message = new MailMessage(new MailAddress(fromEmailAddress, fromEmailDisplayName), new MailAddress(myEmail, myFullName));
                    message.Subject = "AlliedBreeze - Password reset";
                    message.IsBodyHtml = true;
                    message.Body = body;

                    var client = new SmtpClient();
                    client.Credentials = new NetworkCredential(fromEmailAddress, fromEmailPassword);
                    client.Host = smtpHost;
                    client.EnableSsl = true;
                    client.Port = !string.IsNullOrEmpty(smtpPort) ? Convert.ToInt32(smtpPort) : 0;
                    client.Send(message);
                }
                catch (Exception ex)
                {
                    throw (new Exception("Mail send failed to loginId " + ex) );
                }
            }
              
        }
    }
}