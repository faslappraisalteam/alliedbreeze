﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="executive.aspx.cs" Inherits="AlliedBreeze.executive" %>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>AlliedBreeze</title>
    <meta name="viewport" content="">
    <meta name="description" content="">
    <meta property="og:url" content="">
    <meta property="og:type" content="">
    <meta property="og:title" content="">
    <meta property="og:description" content="">
    <meta property="og:image" content="">
    <meta name="twitter:card" content="">
    <meta name="twitter:site" content="">
    <meta name="twitter:creator" content="">
    <meta name="twitter:title" content="">
    <meta name="twitter:description" content="">
    <meta name="twitter:image" content="">
    <link rel="apple-touch-icon" sizes="180x180" href="">
    <link rel="icon" type="image/png" href="" sizes="32x32">
    <link rel="icon" type="image/png" href="" sizes="16x16">
    <link rel="manifest" href="manifest.json">
    <link rel="mask-icon" href="safari-pinned-tab.svg" color="#001C98">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,400italic,500,700">
    <link rel="stylesheet" href="../contents/css/vendor.min.css">
    <link rel="stylesheet" href="../contents/css/elephant.min.css">
    <link rel="stylesheet" href="../contents/css/application.min.css">
      <link rel="stylesheet" href="../contents/css/demo.min.css">
  </head>
  <body class="layout layout-header-fixed">
    <div class="layout-header">
      <div class="navbar navbar-default" style = "background-color: #001C98; height:75px">
        <div class="navbar-header" style = "background-color: #001C98">
          <a class="navbar-brand navbar-brand-center" href="audience.html">
            <img class="navbar-brand-logo" src="../contents/img/loo.png" alt="" style = "margin-top: -15px; height:72px; width:76px">
          </a>
          <button class="navbar-toggler visible-xs-block collapsed" type="button" data-toggle="collapse" data-target="#sidenav">
            <span class="sr-only">Toggle navigation</span>
            <span class="bars">
              <span class="bar-line bar-line-1 out"></span>
              <span class="bar-line bar-line-2 out"></span>
              <span class="bar-line bar-line-3 out"></span>
            </span>
            <span class="bars bars-x">
              <span class="bar-line bar-line-4"></span>
              <span class="bar-line bar-line-5"></span>
            </span>
          </button>
          <button class="navbar-toggler visible-xs-block collapsed" type="button" data-toggle="collapse" data-target="#navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="arrow-up"></span>
            <span class="ellipsis ellipsis-vertical">
              <img class="ellipsis-object" width="32" height="32" src="../contents/img/user.png" alt="Teddy Wilson">
            </span>
          </button>
        </div>
        <div class="navbar-toggleable">
          <nav id="navbar" class="navbar-collapse collapse">
            <button class="sidenav-toggler hidden-xs" title="Collapse sidenav ( [ )" aria-expanded="true" type="button">
              <span class="sr-only">Toggle navigation</span>
              <span class="bars">
                <span class="bar-line bar-line-1 out"></span>
                <span class="bar-line bar-line-2 out"></span>
                <span class="bar-line bar-line-3 out"></span>
                <span class="bar-line bar-line-4 in"></span>
                <span class="bar-line bar-line-5 in"></span>
                <span class="bar-line bar-line-6 in"></span>
              </span>
            </button>
            <ul class="nav navbar-nav navbar-right">
              <li class="visible-xs-block">
                <h4 class="navbar-text text-center">Hi, Ama Mantebea</h4>
              </li>


              <li class="dropdown hidden-xs">
                <button class="navbar-account-btn" data-toggle="dropdown" aria-haspopup="true" style="margin-top:15px">
                  <img class="rounded" width="36" height="36" src="contents/img/user.png" alt=""> &nbsp &nbsp Ama Mantebea | <a href="login-1.html"><span style = "color: #fff"><i class = "icon icon-lock"> </i> Sign out</span></a>

                </button>


              </li>


              <li class="visible-xs-block">
                <a href="login-1.html">
                  <span class="icon icon-power-off icon-lg icon-fw"></span>
                  Sign out
                </a>
              </li>
            </ul>
          </nav>
        </div>
      </div>
    </div>

    <div class="layout-main" >
      <div class="layout-sidebar" >
        <div class="layout-sidebar-backdrop" style="background-color:white"></div>
        <div class="layout-sidebar-body" style="background-color:white">
          <div class="custom-scrollbar">
            <nav id="sidenav" class="sidenav-collapse collapse" style="background-color:white">
              <ul class="sidenav">
                  <br />
                <li class="sidenav-heading">Analytics</li>
                <li class="sidenav-item  active">
                  <a href="dashboard-1.html" aria-haspopup="true">
                    <span class="sidenav-icon icon icon-bar-chart"></span>
                    <span class="sidenav-label" style="font-size: 11px">Dashboard</span>
                  </a>
                                 </li>
               <li class="sidenav-item  ">
                  <a href="dashboard-1.html" aria-haspopup="true">
                    <span class="sidenav-icon icon icon-area-chart"></span>
                    <span class="sidenav-label" style="font-size: 11px">CI Portfolio Summary</span>
                  </a>
                                 </li>
               
                   

               



              </ul>
            </nav>
          </div>
        </div>
      </div>
      <div class="layout-content">
        <div class="layout-content-body">
        
        </div>
      </div>

    </div>
  


    <script src="../contents/js/vendor.min.js"></script>
    <script src="../contents/js/elephant.min.js"></script>
    <script src="../contents/js/application.min.js"></script>
    <script src="../contents/js/demo.min.js"></script>
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
      ga('create', 'UA-83990101-1', 'auto');
      ga('send', 'pageview');
    </script>
  </body>
</html>